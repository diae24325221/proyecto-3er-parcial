from fastapi import FastAPI
from config.database import Session, engine, Base

from middlewares.error_handler import ErrorHandler
from routes.books import books_router
from routes.categories import categories_router
from routes.users import users_router
from fastapi.middleware.cors import CORSMiddleware

# from error_handler import ErrorHandler
# from fastapi.middleware.wsgi import add_middleware

app = FastAPI()

app.add_middleware(CORSMiddleware, allow_origins=["*"], allow_credentials=True, allow_methods=["*"], allow_headers=["*"])

# app.add_middleware(ErrorHandler)
app.title = "PROYECTO BIBLIOTECA API Te quiero mucho profe jair" 

app.add_middleware(ErrorHandler)
app.include_router(books_router)
app.include_router(categories_router)
app.include_router(users_router)    

Base.metadata.create_all(bind=engine)


